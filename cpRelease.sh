# Set Src Directory
srcDir='/Users/jptacek/Src/SkyDev/USV-VoiceSearch-POC/build'

echo Starting....
#Copy assets
srcDirImages=$srcDir'/images/*'
cp -vR $srcDir/images/* ./images
cp -vR $srcDir/script/* ./script
cp -vR $srcDir/index.html .
echo
echo
# Validate version
cat index.html | grep 'v 0.'

echo Done....
