var Stamp = Stamp || {};

(function(ns) {

  // Utilty object to capture the document context of the script
  // tag that instantiated the Context. Useful for getting access to
  // assetts in HTML Imports.
  ns.Context = function() {
      this.doc = document;
  };

  // Import the element 'id' from the HTML Import into the current docuemnt.
  ns.Context.prototype.import = function(id) {
    return document.importNode(this.doc.querySelector('#'+id).content, true);
  };

  // Utiltity function for creating a custom element.
  //
  // name - The name of the custom element.
  // proto - And object to use as the elements prototype.
  ns.newElement = function(name, proto) {
    var ep = Object.create(HTMLElement.prototype);
    Object.keys(proto).forEach(function(key) {
      ep[key] = proto[key];
    });
    var __ = ep.__ = {};
    __.context = new ns.Context();
    __.name = name;
    document.registerElement(name, {
      prototype: ep
    });
  }

  // Regex to grab double moustache'd content.
  var re = /{{\s([\w\.\^]+)\s}}/g;

  // Returns 'state' after applying address, where address is an array of
  // string.
  //
  // I.e. if the state is {a: {b: [9, 8, 7]}} then the address of ["a", "b",
  // 0] would return 9.
  function filterState(address, state) {
    var mystate = state;
    for (var i = 0, len = address.length; i < len; i++) {
      var a = address[i];
      if (a in mystate) {
        mystate = mystate[a];
      } else if (mystate.hasAttribute && mystate.hasAttribute(a)) {
        mystate = mystate.getAttribute(a);
      } else {
        throw a + " is not a valid property of " + JSON.stringify(mystate);
      }
    }
    return mystate;
  }

  // Replace the content in 'str' from [index, index+count) with 'add'.
  function ssplice(str, index, count, add) {
    return str.slice(0, index) + add + str.slice(index + count);
  }

  // Extracts an address path, i.e. ["a", "b", "0"] from the input string 's',
  // such as from "{{a.b.0}}".
  function addressOf(s) {
    if ((match = re.exec(s)) != null) {
      return match[1].split(".");
    } else {
      return null;
    }
  }

  // Keeps expanding double moustache in the text content until there no more
  // double moustache instances. Returns null if no templates were found,
  // otherwise returns the expanded string.
  function expandString(s, state) {
    var match;
    var matches = [];

    // Flush re's cache of the last string it was parsing.
    re.exec("");
    // Find all the matches in s.
    while ((match = re.exec(s)) != null) {
      matches.push(match);
    }

    for (var i = matches.length - 1; i >= 0; i--) {
      match = matches[i];
      var address = match[1].split(".");
      var m = filterState(address, state);
      s = ssplice(s, match.index, match[0].length, m);
    }
    if (matches.length) {
      return s;
    }
    return null;
  }

  // Takes an array of nodes and returns an array of cloned nodes in the same
  // order.
  function cloneAllNodes(a) {
    var clones = [];
    for (var i = 0, len = a.length; i < len; i++) {
      if (a[i].nodeName == "TEMPLATE") {
        // Template nodes have their contents hoisted up to this level for expansion.
        clones.push(a[i].content.cloneNode(true));
      } else {
        clones.push(a[i].cloneNode(true));
      }
    }
    return clones;
  }

  // Append all the elements in 'nodes' as children of 'ele'.
  function appendChildren(ele, nodes) {
    for (var i = 0, len = nodes.length; i < len; i++) {
      ele.appendChild(nodes[i]);
    }
  }

  // Removes all the children of 'ele'.
  function removeChildren(ele) {
    ele.innerHTML = "";
  }

  // Removes all the children of 'ele' and then adds 'nodes' as ele's
  // children.
  function replaceChildren(ele, nodes) {
    ele.innerHTML = "";
    appendChildren(ele, nodes);
  }

  // Expand all the double moustaches found in the node
  // 'ele' and all its children against the data in 'state'.
  //
  // When descending into nested templates, i.e. a data-repeat- template then
  // '^' is added to the child state as a way to access the data in the
  // parent's scope.
  function expand(ele, state) {
    if (ele.nodeName === "#text") {
      m = expandString(ele.textContent, state);
      if (m != null) {
        ele.textContent = m;
      }
      return ele;
    }
    if (!Array.isArray(ele)) {
      ele = [ele];
    }
    for (var j = 0, len = ele.length; j < len; j++) {
      var e = ele[j];
      var processChildren = true;
      if (e.nodeName === "#text") {
        m = expandString(e.textContent, state);
        if (m != null) {
          e.textContent = m;
        }
      } else {
        if (e.attributes != undefined) {
          for (var i = e.attributes.length-1; i >= 0; i--) {
            var attr = e.attributes[i];
            if (attr.name.indexOf('data-repeat') === 0) {
              processChildren = false;
              var parts = attr.name.split('-');
              if (parts.length !== 3 && parts.length !== 4) {
                throw "Repeat format is data-repeat-<name>[-<iterName>]. Got " + attr.name;
              }
              var name = parts[2];
              var iterName = parts[3];
              var tpl = [];
              while (e.firstChild) {
                tpl.push(e.removeChild(e.firstChild));
              }
              var address = [attr.value];
              if (attr.value.indexOf("}}") !== -1) {
                address = addressOf(attr.value);
              }
              if (address === null) {
                throw attr.value + " doesn't contain an address.";
              }
              var childState = filterState(address, state);
              var instanceState = {
                "^": state,
              };
              if (Object.prototype.toString.call( childState) === '[object Array]') {
                iterName = iterName || "i";
                for (var k = 0; k < childState.length; k++) {
                  var cl = cloneAllNodes(tpl);
                  instanceState[name] =  childState[k];
                  instanceState[iterName] = k;
                  expand(cl, instanceState);
                  appendChildren(e, cl);
                }
              } else {
                iterName = iterName || "key";
                var keys = Object.keys(childState).sort();
                for (var m = 0; m < keys.length; m++) {
                  var key = keys[m];
                  var cl = cloneAllNodes(tpl);
                  instanceState[name] = childState[key];
                  instanceState[iterName] = key;
                  expand(cl, instanceState);
                  appendChildren(e, cl);
                }
              }
              // Remove the data-repeat-* attribute.
              e.removeAttribute(attr.name);
            } else {
              m = expandString(attr.value, state);
              if (m != null) {
                var name = attr.name;
                // Strip trailing "-" from attribute names. This allows
                // setting attributes like src on an img where you would't
                // want the unexpanded value to be used by the browser.
                if (name.charAt(name.length-1) == "-") {
                  e.removeAttribute(attr.name);
                  e.setAttribute(attr.name.slice(0, -1), m);
                } else {
                  attr.value = m;
                }
              }
            }
          }
        }
      }
      if (processChildren) {
        var childEle = e.firstChild;
        while (childEle != null) {
          var nextSibling = childEle.nextSibling;
          if (childEle.nodeName == "TEMPLATE") {
            // If this is a template we need to expand the content of the
            // template node, then replace the template node with the expanded
            // content.
            var replacement = expand(childEle.content.cloneNode(true), state);
            while (replacement[0].childNodes.length > 0) {
              e.insertBefore(replacement[0].firstChild, childEle);
            }
            // Finally remove the original element.
            e.removeChild(childEle);
          } else {
            expand(childEle, state);
          }
          childEle = nextSibling;
        }
      }
    }
    return ele;
  };

  // Expand the template 'ele' with 'state' and then replace all the children
  // of 'target' with the expanded content.
  function expandInto(target, ele, state) {
    replaceChildren(target, expand(ele, state));
  }

  // <host-content> acts similar to the classic <content select="[css selector]"></content>,
  // but as of today <content> isn't standardized and may change in the
  // future, so we use our own custom element for now.
  ns.newElement('host-content', {});

  // Look for <host-content> elements in target and use their select's to pick
  // nodes from source to replace it with.
  function distribute(targets, source) {
    if (!Array.isArray(targets)) {
      targets = [targets];
    }
    for (var i = targets.length - 1; i >= 0; i--) {
      var target = targets[i];
      var nodes = target.querySelectorAll('host-content');
      for (var i=nodes.length-1; i>=0; i--) {
        var node = nodes.item(i);
        var parent = node.parentNode;
        var select = node.getAttribute('select');
        if (select) {
          var sources = source.querySelectorAll(select);
          for (var j=sources.length-1; j>=0; j--) {
            parent.insertBefore(sources.item(j), node);
          }
        }
        parent.removeChild(node);
      }
    }
    return targets;
  };

  // Expand ele with the values in state, then distribute
  // nodes in source within the result.
  function expandAndDistribute(ele, state, source) {
    return distribute(expand(ele, state), source);
  };

  // state is optional, if not provided then 'ele' is used.
  // id is optional, if not provided then ele.__.name is used.
  // Presumes that ele is a custom element created with newElement, i.e.
  // that ele.__.context will be a Context object, and that
  // ele.__.name will be the element name.
  function elementExpand(ele, id, state) {
    if (state === undefined) {
      state = ele;
    }
    if (id === undefined) {
      id = ele.__.name;
    }
    var d = ns.expandAndDistribute(ele.__.context.import(id), state, ele);
    ns.appendChildren(ele, d);
  }

  ns.appendChildren = appendChildren;
  ns.expand = expand;
  ns.distribute = distribute;
  ns.expandAndDistribute = expandAndDistribute;
  ns.elementExpand = elementExpand;
  ns.expandInto = expandInto;
})(Stamp);


//----------------------------------------------------------------------
// Microsoft Speech SDK
// ====================
// 
// 
// FEATURES
// --------
// * Short-form recognition.
// * Long-form dictation.
// * Recognition with intent.
// * Integrated microphone support.
// * External audio support.
// 
// LICENSE
// -------
// � 2015 Microsoft. All rights reserved.  
// This document is provided �as-is�. Information and views expressed in this document, including URL and other Internet Web site references, may change without notice.  
// Some examples depicted herein are provided for illustration only and are fictitious.  No real association or connection is intended or should be inferred. 
// This document does not provide you with any legal rights to any intellectual property in any Microsoft product. You may copy and use this document for your internal, reference purposes. This 
// document is confidential and proprietary to Microsoft. It is disclosed and can be used only pursuant to a non-disclosure agreement. 
//----------------------------------------------------------------------

var Microsoft;
(function (Microsoft) {
    (function (CognitiveServices) {
        (function (SpeechRecognition) {
            (function (SpeechRecognitionMode) {
                SpeechRecognitionMode._map = [];
                SpeechRecognitionMode._map[0] = "shortPhrase";
                SpeechRecognitionMode.shortPhrase = 0;
                SpeechRecognitionMode._map[1] = "longDictation";
                SpeechRecognitionMode.longDictation = 1;
            })(SpeechRecognition.SpeechRecognitionMode || (SpeechRecognition.SpeechRecognitionMode = {}));
            var SpeechRecognitionMode = SpeechRecognition.SpeechRecognitionMode;
            var MicrophoneRecognitionClient = (function () {
                function MicrophoneRecognitionClient(prefs) {
                    this.onPartialResponseReceived = null;
                    this.onFinalResponseReceived = null;
                    this.onIntentReceived = null;
                    this.onError = null;
                    this._prefs = prefs;
                    this._sr = new Bing.Speech();
                }
                MicrophoneRecognitionClient.prototype.startMicAndRecognition = function () {
                    var _this = this;
                    this._sr.onresult = function (e) {
                        if (e.results[e.resultIndex].final) {
                            _this.onFinalResponseReceived(e.results[e.resultIndex]);
                        } else {
                            _this.onPartialResponseReceived(e.results[e.resultIndex][0].transcript);
                        }
                    };
                    this._sr.onerror = function (e) {
                        if (_this.onError) {
                            _this.onError(-1, JSON.stringify(e));
                        }
                    };
                    this._sr.addEventListener("audioend", function () {
                        console.log("audio has ended");
                    }, true);
                    Bing.Platform.getCU().done(function (cu) {
                        cu.preferences = _this._prefs;
                        cu.onintent = function (intent) {
                            if (_this.onIntentReceived) {
                                _this.onIntentReceived(intent.payload);
                            }
                        };
                        _this._sr.start();
                    });
                };
                MicrophoneRecognitionClient.prototype.endMicAndRecognition = function () {
                    this._sr.stop();
                };
                return MicrophoneRecognitionClient;
            })();
            SpeechRecognition.MicrophoneRecognitionClient = MicrophoneRecognitionClient;            
            var DataRecognitionClient = (function () {
                function DataRecognitionClient(prefs) {
                    this.onPartialResponseReceived = null;
                    this.onFinalResponseReceived = null;
                    this.onIntentReceived = null;
                    this.onError = null;
                    this._prefs = prefs;
                    this._start = true;
                    this._sr = new Bing.Speech();
                }
                DataRecognitionClient.prototype.sendAudio = function (buffer, actualAudioBytesInBuffer) {
                    var _this = this;
                    var src = new Bing.ArrayBufferSource();
                    src.setBuffer(buffer);
                    this._sr.mediaSource = src;
                    this._sr.onresult = function (e) {
                        if (e.results[e.resultIndex].final) {
                            _this.onFinalResponseReceived(e.results[e.resultIndex]);
                        } else {
                            _this.onPartialResponseReceived(e.results[e.resultIndex][0].transcript);
                        }
                    };
                    this._sr.onerror = function (e) {
                        if (_this.onError) {
                            _this.onError(-1, JSON.stringify(e));
                        }
                    };
                    Bing.Platform.getCU().done(function (cu) {
                        cu.preferences = _this._prefs;
                        cu.onintent = function (intent) {
                            if (_this.onIntentReceived) {
                                _this.onIntentReceived(intent.payload);
                            }
                        };
                        _this._sr.start();
                    });
                };
                DataRecognitionClient.prototype.endAudio = function () {
                    this._start = true;
                };
                return DataRecognitionClient;
            })();
            SpeechRecognition.DataRecognitionClient = DataRecognitionClient;            
            (function (SpeechRecognitionServiceFactory) {
                function createDataClient(speechRecognitionMode, language, primaryKey, secondaryKey) {
                    return new SpeechRecognition.DataRecognitionClient(createPrefs(speechRecognitionMode, language, primaryKey));
                }
                SpeechRecognitionServiceFactory.createDataClient = createDataClient;
                function createDataClientWithIntent(language, primaryKey, luisAppId, luisSubscriptionId) {
                    var prefs = createPrefs(SpeechRecognition.SpeechRecognitionMode.shortPhrase, language, primaryKey);
                    prefs.luisAppId = luisAppId;
                    prefs.luisSubscriptionId = luisSubscriptionId;
                    return new SpeechRecognition.DataRecognitionClient(prefs);
                }
                SpeechRecognitionServiceFactory.createDataClientWithIntent = createDataClientWithIntent;
                function createMicrophoneClient(speechRecognitionMode, language, primaryKey, secondaryKey) {
                    return new SpeechRecognition.MicrophoneRecognitionClient(createPrefs(speechRecognitionMode, language, primaryKey));
                }
                SpeechRecognitionServiceFactory.createMicrophoneClient = createMicrophoneClient;
                function createMicrophoneClientWithIntent(language, primaryKey, luisAppId, luisSubscriptionId) {
                    var prefs = createPrefs(SpeechRecognition.SpeechRecognitionMode.shortPhrase, language, primaryKey);
                    prefs.luisAppId = luisAppId;
                    prefs.luisSubscriptionId = luisSubscriptionId;
                    return new SpeechRecognition.MicrophoneRecognitionClient(prefs);
                }
                SpeechRecognitionServiceFactory.createMicrophoneClientWithIntent = createMicrophoneClientWithIntent;
                SpeechRecognitionServiceFactory.BaseSpeechUrl = "https://speech.platform.bing.com/recognize";
                function createPrefs(speechRecognitionMode, language, primaryKey) {
                    var serviceUri = SpeechRecognitionServiceFactory.BaseSpeechUrl;
                    switch(speechRecognitionMode) {
                        case SpeechRecognition.SpeechRecognitionMode.longDictation:
                            throw "SpeechRecognitionMode.longDictation is not a currently supported mode.";
                    }
                    return {
                        serviceUri: serviceUri,
                        locale: language,
                        clientId: primaryKey,
                        clientVersion: "4.0.150429",
                        authenticationScheme: "MAIS"
                    };
                }
                SpeechRecognitionServiceFactory.createPrefs = createPrefs;
            })(SpeechRecognition.SpeechRecognitionServiceFactory || (SpeechRecognition.SpeechRecognitionServiceFactory = {}));
            var SpeechRecognitionServiceFactory = SpeechRecognition.SpeechRecognitionServiceFactory;
        })(CognitiveServices.SpeechRecognition || (CognitiveServices.SpeechRecognition = {}));
        var SpeechRecognition = CognitiveServices.SpeechRecognition;
    })(Microsoft.CognitiveServices || (Microsoft.CognitiveServices = {}));
    var CognitiveServices = Microsoft.CognitiveServices;
})(Microsoft || (Microsoft = {}));
var Bing;
(function (Bing) {
    Bing._cu;
    Bing._cuDeferred = [];
    Bing._window = window;
    Bing._defaultVoiceName = "Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)";
    function write(text) {
        if (Bing._window.onlog) {
            Bing._window.onlog(text);
        }
        if (console && console.log) {
            console.log(text);
        }
    }
    Bing.write = write;
    function writeline(text) {
        write(text + "\n");
    }
    Bing.writeline = writeline;
    function setValue(name, value) {
        var json = JSON.stringify(value);
        window.localStorage.setItem(name, json);
    }
    Bing.setValue = setValue;
    function getValue(name) {
        var json = window.localStorage.getItem(name);
        var ret;
        if (json !== null && json != 'undefined') {
            ret = JSON.parse(json);
        }
        return ret;
    }
    Bing.getValue = getValue;
    function dispatchEvent(name) {
        var event = document.createEvent('Event');
        event.initEvent(name, true, true);
        window.dispatchEvent(event);
    }
    Bing.dispatchEvent = dispatchEvent;
    function dispatchAudioStart() {
        dispatchEvent('audiostart');
    }
    Bing.dispatchAudioStart = dispatchAudioStart;
    function dispatchAudioStop() {
        dispatchEvent('audiostop');
    }
    Bing.dispatchAudioStop = dispatchAudioStop;
    function useHttp() {
        return getValue("useHttp");
    }
    Bing.useHttp = useHttp;
    function devMode() {
        return getValue("devMode");
    }
    Bing.devMode = devMode;
    (function (SynthState) {
        SynthState._map = [];
        SynthState._map[0] = "None";
        SynthState.None = 0;
        SynthState._map[1] = "Pending";
        SynthState.Pending = 1;
        SynthState._map[2] = "Started";
        SynthState.Started = 2;
    })(Bing.SynthState || (Bing.SynthState = {}));
    var SynthState = Bing.SynthState;
    var Synthesis = (function () {
        function Synthesis() {
            var _this = this;
            this.voices = [];
            this._state = SynthState.None;
            this.paused = false;
            this.voices = null;
            var request = new XMLHttpRequest();
            request.open('GET', "https://speech.platform.bing.com/synthesize/list/voices", true);
            request.responseType = 'json';
            request.onload = function () {
                if (request.readyState == 4) {
                    _this.voices = [];
                    if (request.status === 200) {
                        var list = handleJSONWebResponse(request);
                        for(var i = 0; i < list.length; ++i) {
                            var vox = {
                                voiceURI: "https://speech.platform.bing.com/synthesize",
                                name: list[i].Name,
                                lang: list[i].Locale,
                                localService: false,
                                default: list[i].Name == Bing._defaultVoiceName
                            };
                            _this.voices.push(vox);
                            if (vox.default) {
                                _this.defaultVoice = vox;
                            }
                        }
                    }
                }
            };
            request.send();
        }
        Object.defineProperty(Synthesis.prototype, "pending", {
            get: function () {
                return this._state == SynthState.Pending;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Synthesis.prototype, "speaking", {
            get: function () {
                return this._state == SynthState.Started;
            },
            enumerable: true,
            configurable: true
        });
        Synthesis.prototype.speak = function (utterance) {
            var _this = this;
            var start = Date.now();
            this._state = SynthState.Pending;
            Platform.getCU().done(function (cu) {
                cu.onttsstart = function () {
                    _this._state = SynthState.Started;
                    if (utterance.onstart) {
                        utterance.onstart(_this.createEvent("start", utterance));
                    }
                };
                cu.onttsend = function () {
                    _this._state = SynthState.None;
                    if (utterance.onend) {
                        var e = _this.createEvent("end", utterance);
                        e.elapsedTime = (Date.now() - start) / 1000;
                        utterance.onend(e);
                    }
                };
                cu.onevent = function () {
                    _this._state = SynthState.None;
                    utterance.onerror(_this.createEvent("error", utterance));
                };
                if (!_this.context) {
                    if (!AudioContext) {
                        throw "Sorry, your browser doesn't support WebAudio";
                    }
                    _this.context = new AudioContext();
                }
                cu.context = _this.context;
                cu.tts(utterance.text);
            });
        };
        Synthesis.prototype.cancel = function () {
            this._state = SynthState.None;
            Platform.getCU().done(function (cu) {
                cu.ttsStop();
            });
        };
        Synthesis.prototype.pause = function () {
        };
        Synthesis.prototype.resume = function () {
        };
        Synthesis.prototype.getVoices = function () {
            return this.voices;
        };
        Synthesis.prototype.createEvent = function (type, target) {
            var e = document.createEvent("UIEvent");
            e.initUIEvent(type, false, false, null, null);
            e.target = e.currentTarget = target;
            e.srcElement = target;
            e.timeStamp = Date.now();
            return e;
        };
        return Synthesis;
    })();
    Bing.Synthesis = Synthesis;    
    var msSpeechSynthesisUtterance = (function () {
        function msSpeechSynthesisUtterance(text) {
            this.text = text;
        }
        msSpeechSynthesisUtterance.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        msSpeechSynthesisUtterance.prototype.addEventListener = function (type, listener, useCapture) {
        };
        msSpeechSynthesisUtterance.prototype.dispatchEvent = function (evt) {
            return false;
        };
        return msSpeechSynthesisUtterance;
    })();    
    var Speech = (function () {
        function Speech() {
            this._firstAudio = true;
            this.playbackAudioFilesOverride = false;
            this.grammars = null;
            this.lang = "en-US";
            this.continuous = false;
            this.interimResults = true;
            this.maxAlternatives = -1;
            this.serviceURI = null;
        }
        Speech.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        Speech.prototype.addEventListener = function (type, listener, useCapture) {
        };
        Speech.prototype.dispatchEvent = function (evt) {
            return false;
        };
        Speech.prototype.start = function () {
            var _this = this;
            if (this.onstart) {
                this.onstart(this.createEvent("start"));
            }
            if (this.isMicSource) {
                if (!(navigator).getUserMedia) {
                    throw "Sorry, your browser doesn't have microphone support.";
                }
                (navigator).getUserMedia({
                    audio: true
                }, function (stream) {
                    if (!_this.context) {
                        if (!AudioContext) {
                            throw "Sorry, your browser doesn't support WebAudio";
                        }
                        _this.context = new AudioContext();
                    }
                    _this.currentSource = _this.context.createMediaStreamSource(stream);
                }, function () {
                    _this.HandleError();
                });
            } else {
                this.currentSource = this.mediaSource;
            }
        };
        Speech.prototype.stop = function () {
            if (this._currentSource) {
                Platform.getCU().done(function (cu) {
                    cu.disconnect();
                });
                if (this._currentSource) {
                    this._currentSource.disconnect();
                }
                this._currentSource = null;
            }
            if (this._currentDestination) {
                this._currentDestination.disconnect();
                this._currentDestination = null;
            }
        };
        Speech.prototype.abort = function () {
            this.stop();
        };
        Speech.prototype.HandleError = function () {
        };
        Object.defineProperty(Speech.prototype, "currentSource", {
            set: function (source) {
                var _this = this;
                this._firstAudio = true;
                this._currentSource = source;
                Platform.getCU().done(function (cu) {
                    cu.onend = function () {
                        if (_this.onend) {
                            _this.onend(_this.createEvent("end"));
                        }
                    };
                    cu.onaudioend = function () {
                        if (_this.onaudioend) {
                            _this.onaudioend(_this.createEvent("audioend"));
                        }
                    };
                    cu.ondisplaytext = function (text) {
                        if (_this.onresult) {
                            var e = _this.createEvent("result");
                            e.resultIndex = 0;
                            e.results = {
                                length: 1,
                                0: {
                                    final: false,
                                    length: 1,
                                    0: {
                                        transcript: text
                                    }
                                }
                            };
                            _this.onresult(e);
                        }
                    };
                    cu.onresult = function (result) {
                        if (result.status == 301) {
                            if (_this.onnomatch) {
                                _this.onnomatch(_this.createEvent("nomatch"));
                            }
                        } else if (_this.onresult) {
                            _this.onresult(result);
                        } else {
                            console.warn("Speech.onresult not set");
                        }
                    };
                    cu.onevent = function (statusCode) {
                        if (_this.onerror) {
                            var e = _this.createEvent("error");
                            e.error = "received a speech error " + statusCode;
                            _this.onerror(e);
                        }
                        if (_this.onend) {
                            _this.onend(_this.createEvent("end"));
                        }
                    };
                    _this._currentDestination = _this.createRecogitionDestination(_this._currentSource, cu, null);
                });
            },
            enumerable: true,
            configurable: true
        });
        Speech.prototype.createEvent = function (type) {
            var e = document.createEvent("UIEvent");
            e.initUIEvent(type, false, false, null, null);
            e.target = e.currentTarget = this;
            e.srcElement = this;
            e.timeStamp = Date.now();
            return e;
        };
        Speech.prototype.createRecogitionDestination = function (source, cu, onaudioprocess) {
            var _this = this;
            var destination = source.context.createScriptProcessor(4096, 1, 1);
            destination.onaudioprocess = function (e) {
                var inputBuffer = e.inputBuffer;
                if (_this._firstAudio) {
                    _this._firstAudio = false;
                    if (_this.onaudiostart) {
                        _this.onaudiostart(_this.createEvent("audiostart"));
                    }
                }
                if (Bing._window.useStringArrays === true) {
                    cu.audioprocess(new StringAudioBuffer(inputBuffer));
                } else {
                    cu.audioprocess(inputBuffer);
                }
                if (onaudioprocess) {
                    onaudioprocess(e);
                }
                if (_this.playbackAudioFiles() && !_this.isMicSource) {
                    var outputBuffer = e.outputBuffer;
                    for(var channel = 0; channel < outputBuffer.numberOfChannels; channel++) {
                        var outputData = outputBuffer.getChannelData(channel);
                        var inputData = inputBuffer.getChannelData(channel);
                        for(var sample = 0; sample < e.inputBuffer.length; sample++) {
                            outputData[sample] = inputData[sample];
                        }
                    }
                }
                if (Bing._window.msSpeechButton && !Bing._window.isActiveX) {
                    Bing._window.msSpeechButton.audioprocess(e);
                }
            };
            cu.connect(source);
            destination.connect(source.context.destination);
            source.connect(destination);
            return destination;
        };
        Object.defineProperty(Speech.prototype, "isMicSource", {
            get: function () {
                return null === this.mediaSource || typeof this.mediaSource == 'undefined';
            },
            enumerable: true,
            configurable: true
        });
        Speech.prototype.playbackAudioFiles = function () {
            return getValue("playbackAudioFiles") || this.playbackAudioFilesOverride;
        };
        return Speech;
    })();
    Bing.Speech = Speech;    
    ;
    var Task = (function () {
        function Task() {
            this.completed = false;
            this.startTime = Date.now();
        }
        Task.prototype.complete = function () {
            this.signalComplete(true);
        };
        Task.prototype.resolve = function (result) {
            this.signalComplete(result);
        };
        Task.prototype.done = function (cb) {
            if (this.completed) {
                cb(this.result);
                return;
            }
            this.cb = cb;
        };
        Object.defineProperty(Task.prototype, "elapsedTime", {
            get: function () {
                return (Date.now() - this.startTime) / 1000;
            },
            enumerable: true,
            configurable: true
        });
        Task.prototype.signalComplete = function (result) {
            this.result = result;
            this.completed = true;
            if (this.cb) {
                this.cb(this.result);
            }
        };
        return Task;
    })();
    Bing.Task = Task;    
    var StringAudioBuffer = (function () {
        function StringAudioBuffer(buffer) {
            this._audioBuffer = buffer;
            this.sampleRate = buffer.sampleRate;
            this.length = buffer.length;
            this.duration = buffer.duration;
            this.numberOfChannels = buffer.numberOfChannels;
        }
        StringAudioBuffer.prototype.getChannelData = function (channel) {
            var data = this._audioBuffer.getChannelData(channel);
            var a = [];
            var ret;
            var i;
            var byteStr;
            for(i = 0; i < data.length; ++i) {
                byteStr = Math.floor((data[i] + 1.0) * 0x7fff).toString(16).replace('-', '');
                while(byteStr.length < 4) {
                    byteStr = "0" + byteStr;
                }
                a.push(byteStr);
            }
            ret = a.join("");
            return ret;
        };
        StringAudioBuffer.prototype.copyFromChannel = function (destination, channelNumber, startInChannel) {
            this._audioBuffer.copyFromChannel(destination, channelNumber, startInChannel);
        };
        StringAudioBuffer.prototype.copyToChannel = function (source, channelNumber, startInChannel) {
            this._audioBuffer.copyToChannel(source, channelNumber, startInChannel);
        };
        return StringAudioBuffer;
    })();
    Bing.StringAudioBuffer = StringAudioBuffer;    
    function CreateActiveXObject(name) {
        try  {
            return new ActiveXObject(name);
        } catch (e) {
            return null;
        }
    }
    Bing.CreateActiveXObject = CreateActiveXObject;
    (function (Platform) {
        function isEdge() {
            return navigator.userAgent.indexOf("Edge/") != -1;
        }
        Platform.isEdge = isEdge;
        function isSafari() {
            return navigator.userAgent.indexOf("AppleWebKit") != -1;
        }
        Platform.isSafari = isSafari;
        function supportsPPAPI() {
            if (Bing._window.chrome && !isEdge() && navigator.userAgent.indexOf("Chrome/")) {
                return true;
            } else {
                return false;
            }
        }
        Platform.supportsPPAPI = supportsPPAPI;
        function supportsNPAPI() {
            if (!isEdge() && ((navigator.userAgent.indexOf("Firefox") != -1) || isSafari())) {
                return true;
            } else {
                return false;
            }
        }
        Platform.supportsNPAPI = supportsNPAPI;
        function supportsActiveX() {
            if ((navigator.userAgent.indexOf("Trident") != -1)) {
                return true;
            } else {
                return false;
            }
        }
        Platform.supportsActiveX = supportsActiveX;
        function getCU() {
            var task = new Bing.Task();
            if (Bing._cu) {
                task.resolve(Bing._cu);
            } else {
                Bing._cuDeferred.push(task);
            }
            return task;
        }
        Platform.getCU = getCU;
    })(Bing.Platform || (Bing.Platform = {}));
    var Platform = Bing.Platform;
    function initialize() {
        var i;
        Bing._window.useStringArrays = false;
        Bing._cu = createSpeech();
        if (!Bing._cuDeferred) {
            Bing._cuDeferred = [];
        }
        if (Bing._cu) {
            for(i = 0; i < Bing._cuDeferred.length; ++i) {
                Bing._cuDeferred[i].resolve(Bing._cu);
            }
        }
    }
    Bing.initialize = initialize;
    function decodeAudioData(context, audioData, successCallback, errorCallback) {
        if (Bing._window.isActiveX) {
            audioData = new Int8Array(audioData);
        }
        context.decodeAudioData(audioData, successCallback, errorCallback);
    }
    Bing.decodeAudioData = decodeAudioData;
    function handleJSONWebResponse(xhr) {
        if (typeof (xhr.response) == "string") {
            return JSON.parse(xhr.response);
        }
        return xhr.response;
    }
    Bing.handleJSONWebResponse = handleJSONWebResponse;
    var Guid = (function () {
        function Guid() { }
        Guid.generateString = function generateString() {
            return Guid.getRnd4HexOctet() + Guid.getRnd4HexOctet() + "-" + Guid.getRnd4HexOctet() + "-" + Guid.getRndGuidTimeHiAndVersionHex() + "-" + Guid.getRndGuidClockSeqHiAndReservedHex() + "-" + Guid.getRnd4HexOctet() + Guid.getRnd4HexOctet() + Guid.getRnd4HexOctet();
        };
        Guid.generate16bitRnd = function generate16bitRnd() {
            return Math.random() * 0x10000 | 0;
        };
        Guid.getRnd4HexOctet = function getRnd4HexOctet() {
            return ("0000" + Guid.generate16bitRnd().toString(16)).slice(-4);
        };
        Guid.getRndGuidTimeHiAndVersionHex = function getRndGuidTimeHiAndVersionHex() {
            return (Guid.generate16bitRnd() & 0x0FFF | 0x4000).toString(16);
        };
        Guid.getRndGuidClockSeqHiAndReservedHex = function getRndGuidClockSeqHiAndReservedHex() {
            return (Guid.generate16bitRnd() & 0x3FFF | 0x8000).toString(16);
        };
        return Guid;
    })();
    Bing.Guid = Guid;    
    var CognitiveServiceAuthenticator = (function () {
        function CognitiveServiceAuthenticator() {
        }
        CognitiveServiceAuthenticator.prototype.authenticate = function (primaryKey, secondaryKey) {
            var _this = this;
            var task = new Task();
            var now = Date.now();
            if (!this._access_token || !this._expireTime || Date.now() >= this._expireTime.getTime()) {
                writeline("refreshing token");
                var xhr = new XMLHttpRequest();
                xhr.open('POST', "https://api.cognitive.microsoft.com/sts/v1.0/issueToken", true);
                xhr.onload = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status === 200) {
                            _this._access_token = xhr.response;
                            _this._response = JSON.parse(atob(_this._access_token.split(".")[1]));
                            _this._expireTime = new Date(_this._response.exp * 1000);
                            task.resolve("Bearer " + _this._access_token);
                        } else {
                            task.resolve(null);
                        }
                    }
                };
                xhr.onerror = function () {
                    task.resolve(null);
                };
                xhr.setRequestHeader("Ocp-Apim-Subscription-Key", primaryKey);
                xhr.send();
            } else {
                task.resolve("Bearer " + this._access_token);
            }
            return task;
        };
        return CognitiveServiceAuthenticator;
    })();    
    var AdmAuthenticator = (function () {
        function AdmAuthenticator() {
        }
        AdmAuthenticator.prototype.authenticate = function (primaryKey, secondaryKey) {
            var task = new Task();
            writeline("authenticate: " + primaryKey + " " + secondaryKey);
            var params = "grant_type=client_credentials&client_id=" + encodeURIComponent(primaryKey) + "&client_secret=" + encodeURIComponent(secondaryKey) + "&scope=" + encodeURIComponent("https://speech.platform.bing.com");
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13", true);
            xhr.onload = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status === 200) {
                        task.resolve(handleJSONWebResponse(xhr));
                    } else {
                        task.resolve(null);
                    }
                }
            };
            xhr.onerror = function () {
                task.resolve(null);
            };
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(params);
            return task;
        };
        return AdmAuthenticator;
    })();    
    (function (WaveFormat) {
        WaveFormat._map = [];
        WaveFormat.PcmInt = 1;
        WaveFormat.PcmFloat = 3;
    })(Bing.WaveFormat || (Bing.WaveFormat = {}));
    var WaveFormat = Bing.WaveFormat;
    var Riff = (function () {
        function Riff(sampleRate, bitsPerSample, format) {
            this._buffer = [];
            this._channels = 1;
            this._bitsPerSample = bitsPerSample;
            this._sampleRate = sampleRate;
            this.appendString("RIFF");
            this.appendUINT32(0);
            this.appendString("WAVEfmt ");
            this.appendUINT32(2 + 2 + 4 + 4 + 2 + 2);
            this.appendUINT16(format);
            this.appendUINT16(this._channels);
            this.appendUINT32(this._sampleRate);
            this.appendUINT32(this._sampleRate * (this._bitsPerSample >> 3) * this._channels);
            this.appendUINT16(this._bitsPerSample >> 3);
            this.appendUINT16(this._bitsPerSample);

            // TODO: Wrap data chunk after size is known or use sub-chunks?
            this.appendString("data");
            this.appendUINT32(0);
        }
        Riff.prototype.appendString = function (s) {
            for(var i = 0; i < s.length; ++i) {
                this._buffer.push(s.charCodeAt(i));
            }
        };
        Riff.prototype.appendUINT32 = function (n) {
            this.appendUINT16(n);
            this.appendUINT16(n >> 16);
        };
        Riff.prototype.appendUINT16 = function (n) {
            this._buffer.push((n & 0x00ff) >> 0);
            this._buffer.push((n & 0xff00) >> 8);
        };
        Riff.prototype.toByteArray = function () {
            return this._buffer;
        };
        return Riff;
    })();
    var LuisClient = (function () {
        function LuisClient(prefs) {
            this._prefs = prefs;
            switch(prefs.authenticationScheme) {
                case "MAIS":
                    this._auth = new CognitiveServiceAuthenticator();
                    break;
                case "ADM":
                    this._auth = new AdmAuthenticator();
                    break;
            }
        }
        LuisClient.kServiceUrl = "https://api.projectoxford.ai/luis/v1/application?subscription-key=";
        LuisClient.prototype.getIntent = function (text) {
            var task = new Task();
            var request = new XMLHttpRequest();
            request.open('GET', [
                LuisClient.kServiceUrl, 
                this._prefs.luisSubscriptionId, 
                "&id=", 
                this._prefs.luisAppId, 
                "&q=", 
                text
            ].join(""), true);
            request.onload = function () {
                if (request.readyState == 4 && request.status === 200) {
                    var response = handleJSONWebResponse(request);
                    task.resolve(request.response);
                } else {
                    task.resolve(null);
                }
            };
            request.send();
            return task;
        };
        return LuisClient;
    })();
    Bing.LuisClient = LuisClient;    
    var HTTPResultStatus = (function () {
        function HTTPResultStatus() { }
        HTTPResultStatus.SUCCESS = "success";
        HTTPResultStatus.ERROR = "error";
        return HTTPResultStatus;
    })();    
    var HttpClient = (function () {
        function HttpClient(waveFormat) {
            if (typeof waveFormat === "undefined") { waveFormat = WaveFormat.PcmFloat; }
            this.queue = [];
            this.responseFormat = "json";
            writeline("Defaulting to http client");
            this.requestUri = "?scenarios=smd";
            this.requestUri += "&appid=D4D52672-91D7-4C74-8AD8-42B1D98141A5";
            this.requestUri += "&device.os=wp7";
            this.requestUri += "&version=3.0";
            this.requestUri += "&instanceid=" + Bing.Guid.generateString();
            this.waveFormat = waveFormat;
        }
        HttpClient.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        HttpClient.prototype.addEventListener = function (type, listener, useCapture) {
            writeline("addEventListener: " + type + " " + this);
        };
        HttpClient.prototype.dispatchEvent = function (evt) {
            return false;
        };
        HttpClient.prototype.connect = function (destination, output, input) {
            Bing._window.useStringArrays = false;
            this.sourceSampleRate = destination.context.sampleRate;
            this.sampleRate = 16000;
            if (this.sourceSampleRate <= 0) {
                return;
            }
            this.connected = true;
            if (this.preferences.luisAppId && this.preferences.luisSubscriptionId) {
                this.luis = new LuisClient(this.preferences);
            }
            this.queue = [];
            this.buffer = new ArrayBuffer(4096);
            var view = new Uint8Array(this.buffer);
            var riffHeader;
            switch (this.waveFormat) {
                default:
                case WaveFormat.PcmFloat:
                    riffHeader = new Riff(this.sampleRate, 32, WaveFormat.PcmFloat).toByteArray();
                    this.processAudio = this.appendAsFloat32;
                    break;
                case WaveFormat.PcmInt:
                    riffHeader = new Riff(this.sampleRate, 8, WaveFormat.PcmInt).toByteArray();
                    this.processAudio = this.appendAsUInt8;
                    break;
            }
            view.set(riffHeader);
            this.offset = riffHeader.length;
        };
        HttpClient.prototype.disconnect = function () {
            if (this.connected) {
                this.connected = false;
                if (this.onaudioend) {
                    this.onaudioend();
                }
                this.send();
            }
        };
        Object.defineProperty(HttpClient.prototype, "preferences", {
            get: function () {
                return this._preferences;
            },
            set: function (prefs) {
                this._preferences = prefs;
                this.auth = null;
            },
            enumerable: true,
            configurable: true
        });
        HttpClient.prototype.sendText = function (inputText) {
        };
        HttpClient.prototype.audioprocess = function (buffer) {
            this.processAudio(buffer.getChannelData(0));
        };
        HttpClient.prototype.tts = function (text, contentType, outputFormat) {
            var _this = this;
            if (!contentType) {
                contentType = "text/plain";
            }
            if (!outputFormat) {
                outputFormat = "riff-16khz-16bit-mono-pcm";
            }
            if (contentType === "text/plain") {
                text = "<?xml version='1.0' encoding='UTF-8'?>" + "<speak version='1.0' xml:lang='" + this.preferences.locale + "'>" + "<voice xml:lang='" + this.preferences.locale + "' name='" + Bing._defaultVoiceName + "'>" + text + "</voice></speak>";
                contentType = "application/ssml+xml";
            }
            var request = new XMLHttpRequest();
            request.open('POST', "https://speech.platform.bing.com/synthesize", true);
            request.responseType = 'arraybuffer';
            request.setRequestHeader("X-MICROSOFT-OutputFormat", outputFormat);
            request.setRequestHeader("Content-Type", contentType);
            request.onload = function () {
                if (request.readyState == 4 && request.status !== 200) {
                    _this.onevent(request.status);
                } else {
                    _this.renderAudio(_this.context, request.response);
                }
            };
            this.getToken().done(function (token) {
                if (!token) {
                    _this.dispatchError(-1);
                    return;
                }
                request.setRequestHeader("Authorization", token);
                if (_this.onttsstart) {
                    _this.onttsstart();
                }
                request.send(text);
            });
        };
        HttpClient.prototype.ttsStop = function () {
            var src = this.ttsSource;
            if (src && this.context.state != "suspended") {
                try  {
                    src.stop();
                } catch (e) {
                    writeline("ttsStop: buffer source failed to stop. state: " + this.context.state + " exception:" + e);
                }
            }
        };
        HttpClient.prototype.getToken = function () {
            if (!this.auth) {
                switch(this.preferences.authenticationScheme) {
                    case "MAIS":
                        this.auth = new CognitiveServiceAuthenticator();
                        break;
                    case "ADM":
                        this.auth = new AdmAuthenticator();
                        break;
                }
            }
            return this.auth.authenticate(this.preferences.clientId, this.preferences.clientSecret);
        };
        HttpClient.prototype.send = function () {
            var _this = this;
            var result;
            this.getToken().done(function (token) {
                if (!token) {
                    _this.dispatchError(-1);
                    return;
                }
                var serviceUrl = _this.preferences.serviceUri.replace("/ws/speech", "").replace("websockets.", "speech.");
                writeline("connect: url " + serviceUrl);
                var request = new XMLHttpRequest();
                request.open('POST', [
                    serviceUrl, 
                    _this.requestUri, 
                    "&locale=", 
                    _this.preferences.locale, 
                    "&format=", 
                    _this.responseFormat, 
                    "&requestid=", 
                    Bing.Guid.generateString()
                ].join(""), true);
                request.responseType = _this.responseFormat;
                request.setRequestHeader("Content-Type", 'audio/wav; codec="audio/pcm"; samplerate=' + _this.sampleRate);
                request.setRequestHeader("Authorization", token);
                token = token;
                request.onload = function () {
                    if (request.readyState == 4 && request.status !== 200) {
                        _this.dispatchError(request.status);
                    } else {
                        result = handleJSONWebResponse(request);
                        if (result.header.status === HTTPResultStatus.ERROR) {
                            _this.dispatchError(-1);
                        } else {
                            _this.dispatchResult(result);
                        }
                    }
                };
                if (_this.buffer && _this.buffer.byteLength && _this.offset) {
                    var view = new Uint8Array(_this.buffer, 0, _this.offset);
                    request.send(view);
                }
            });
        };
        HttpClient.prototype.dispatchError = function (statusCode) {
            if (this.onevent) {
                this.onevent(statusCode);
            }
        };
        HttpClient.prototype.dispatchResult = function (result) {
            var _this = this;
            var reco;
            if (result.results && result.results.length > 0 && result.results[0].name) {
                reco = result.results[0].name;
            }
            if (this.luis && this.onintent) {
                this.luis.getIntent(reco).done(function (r) {
                    _this.onintent({
                        payload: r
                    });
                });
            }
            if (this.onresult) {
                var phrases = [];
                for(var i = 0; i < result.results.length; ++i) {
                    var r = result.results[i];
                    phrases.push({
                        lexical: r.lexical,
                        display: r.name,
                        inverseNormalization: null,
                        maskedInverseNormalization: null,
                        transcript: r.name,
                        confidence: parseFloat(r.confidence)
                    });
                }
                phrases["final"] = true;
                var finalResult = {
                    resultIndex: 0,
                    results: {
                        length: 1,
                        0: phrases
                    },
                    interpretation: reco,
                    emma: null,
                    status: 200
                };
                this.onresult(finalResult);
            }
            if (this.onend) {
                this.onend();
            }
        };
        HttpClient.prototype.appendAsUInt8 = function (a) {
            var _this = this;
            this.appendData(a, function () {
                return new Int8Array(_this.buffer, _this.offset);
            }, 1, function (s) {
                return Math.floor(s * 128);
            });
        };
        HttpClient.prototype.appendAsFloat32 = function (a) {
            var _this = this;
            this.appendData(a, function () {
                return new Float32Array(_this.buffer, _this.offset);
            }, 4, function (s) {
                return s;
            });
        };
        HttpClient.prototype.appendData = function (a, getViewProvider, outputSampleSize, convertSample) {
            var incrementBy = this.sourceSampleRate / this.sampleRate;
            var requiredElements = ((a.length / incrementBy) | 0) + 1;
            var requiredSpace = requiredElements * outputSampleSize;
            if (this.buffer.byteLength < this.offset + requiredSpace) {
                var newLength = (this.offset + requiredSpace) * 2;
                newLength += newLength % outputSampleSize;
                var newBuffer = new ArrayBuffer(newLength);
                new Uint8Array(newBuffer).set(new Uint8Array(this.buffer));
                this.buffer = newBuffer;
            }
            var view = getViewProvider();
            for(var di = 0, si = 0; si < a.length; ++di, si += incrementBy) {
                view[di] = convertSample(a[Math.floor(si)]);
                this.offset += outputSampleSize;
            }
        };
        HttpClient.prototype.renderAudio = function (context, audioData) {
            var _this = this;
            decodeAudioData(context, audioData, function (buffer) {
                writeline("completed decoding audio");
                _this.ttsSource = context.createBufferSource();
                _this.ttsSource.buffer = buffer;
                _this.ttsSource.connect(_this.context.destination);
                if (_this.onttsstart) {
                    _this.onttsstart();
                }
                _this.ttsSource.start(0);
                _this.ttsSource.onended = function () {
                    if (_this.onttsend) {
                        _this.onttsend();
                    }
                    _this.ttsSource = null;
                };
            }, function () {
                writeline("error decoding audio");
                _this.onevent(-1);
            });
        };
        return HttpClient;
    })();
    Bing.HttpClient = HttpClient;    
    var NaclClient = (function () {
        function NaclClient() {
            var _this = this;
            var chrome = Bing._window.chrome;
            var naclContainer;
            if (devMode() === true) {
                naclContainer = document.createElement("div");
                naclContainer.setAttribute("style", "width:0;height:0");
                naclContainer.addEventListener('load', function (arg) {
                    var i;
                    writeline("plugin load:" + arg);
                    for(i = 0; i < Bing._cuDeferred.length; ++i) {
                        Bing._cuDeferred[i].resolve(Bing._cu);
                    }
                }, true);
                naclContainer.addEventListener('error', function (arg) {
                    writeline("plugin error:" + arg);
                }, true);
                naclContainer.addEventListener('crash', function (arg) {
                    writeline("plugin crash:" + arg);
                }, true);
                naclContainer.addEventListener('message', function (arg) {
                    _this.handleMessage(arg);
                }, true);
                window.document.body.appendChild(naclContainer);
                var npPlugin = document.createElement("embed");
                npPlugin.setAttribute("type", "application/x-pnacl");
                npPlugin.setAttribute("src", "/bin/pepper_speech.nmf");
                npPlugin.setAttribute("id", "pepper_speech");
                this._module = npPlugin;
                naclContainer.appendChild(npPlugin);
            } else {
                var port = chrome.runtime.connect(NaclClient.kKeyId);
                port.onMessage.addListener(function (arg) {
                    _this.handleMessage(arg);
                });
                port.onConnect = function (arg) {
                    writeline("port onConnect:" + arg);
                };
                this._module = port;
            }
        }
        NaclClient.kKeyId = "jffoigoenpgbgnhpchggjapfijhffghe";
        NaclClient.prototype.postMessage = function (arg) {
            if (Bing._window.naclNotInstalled === true) {
                return;
            }
            try  {
                this._module.postMessage(arg);
            } catch (e) {
                if (Bing._window.chrome.runtime.lastError) {
                    Bing._window.naclNotInstalled = true;
                    Bing.initialize();
                }
            }
        };
        NaclClient.prototype.handleMessage = function (arg) {
            var data;
            if ((arg).name) {
                data = arg;
            } else {
                data = arg.data;
            }
            if (this[data.name]) {
                this[data.name](data.data);
            }
        };
        NaclClient.prototype.log = function (arg) {
            var d = new Date();
            Bing.write("[" + d.toISOString() + "] " + arg);
        };
        NaclClient.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        NaclClient.prototype.addEventListener = function (type, listener, useCapture) {
            writeline("addEventListener: " + type + " " + this);
        };
        NaclClient.prototype.dispatchEvent = function (evt) {
            return false;
        };
        NaclClient.prototype.connect = function (destination, output, input) {
            this.postMessage([
                "connect", 
                destination.context.sampleRate
            ]);
        };
        NaclClient.prototype.disconnect = function () {
            this.postMessage([
                "disconnect"
            ]);
        };
        Object.defineProperty(NaclClient.prototype, "preferences", {
            get: function () {
                return this._preferences;
            },
            set: function (prefs) {
                this._preferences = prefs;
                this.postMessage([
                    "setPreferences", 
                    prefs
                ]);
            },
            enumerable: true,
            configurable: true
        });
        NaclClient.prototype.sendText = function (inputText) {
        };
        NaclClient.prototype.audioprocess = function (buffer) {
            this.postMessage([
                "audioprocess", 
                buffer.getChannelData(0), 
                buffer
            ]);
        };
        NaclClient.prototype.tts = function (text, contentType, outputFormat) {
            this.postMessage([
                "tts", 
                text, 
                contentType, 
                outputFormat
            ]);
            if (this.onttsstart) {
                this.onttsstart();
            }
        };
        NaclClient.prototype.ttsStop = function () {
            this.postMessage([
                "ttsstop"
            ]);
        };
        return NaclClient;
    })();
    Bing.NaclClient = NaclClient;    
    function shouldCreateHttp() {
        if (Platform.isEdge()) {
            return true;
        }
        if (((Bing)).checkedForPluginInstall && ((Bing)).checkedForPluginInstall() !== true) {
            return false;
        }
        return true;
    }
    Bing.shouldCreateHttp = shouldCreateHttp;
    function createSpeech() {
        var cu;
        if (useHttp()) {
            return new HttpClient();
        } else if (Bing._window.naclNotInstalled !== true) {
            if (Platform.supportsPPAPI()) {
                Bing._window.chrome.runtime.sendMessage(NaclClient.kKeyId, "__hello", null, function (response) {
                    Bing._window.naclNotInstalled = (response !== "__hello");
                    if (Bing._window.naclNotInstalled) {
                        Bing._cu = new HttpClient();
                    } else {
                        Bing._cu = new NaclClient();
                    }
                    for(var i = 0; i < Bing._cuDeferred.length; ++i) {
                        Bing._cuDeferred[i].resolve(Bing._cu);
                    }
                });
                return null;
            } else if (Platform.supportsNPAPI()) {
                var npPlugin = document.createElement("embed");
                var promise = new Task();
                (npPlugin).type = "application/x-bingspeech";
                (npPlugin).data = "data:application/x-bingspeech,";
                npPlugin.setAttribute("style", "width:0;height:0");
                promise.done(function () {
                    window.document.body.appendChild(npPlugin);
                    try  {
                        (npPlugin)();
                        Bing._cu = npPlugin;
                        (Bing._cu).origin = window.window.location.href;
                        Bing._window.useStringArrays = true;
                    } catch (e) {
                        Bing._cu = new HttpClient();
                    }
                    for(var i = 0; i < Bing._cuDeferred.length; ++i) {
                        Bing._cuDeferred[i].resolve(Bing._cu);
                    }
                });
                if (window.document.body) {
                    promise.complete();
                } else {
                    document.addEventListener('DOMContentLoaded', function () {
                        promise.complete();
                    });
                }
                return null;
            } else if (Platform.supportsActiveX()) {
                cu = CreateActiveXObject("Bing.Host");
                if (cu) {
                    (cu).origin = window.window.location.href;
                    Bing._window.isActiveX = true;
                }
            }
        }
        if (!cu && shouldCreateHttp()) {
            writeline("Defaulting to http client");
            return new HttpClient();
        }
        if (!cu && ((Bing)).SpeechInstaller) {
            var installer = new ((Bing)).SpeechInstaller();
            installer.show("Install the Bing Speech Extender");
            return installer;
        }
        return cu;
    }
    Bing.createSpeech = createSpeech;
    var WebAudioSource = (function () {
        function WebAudioSource(url, context) {
            this.numberOfInputs = 1;
            this.numberOfOutputs = 1;
            this.channelCount = 1;
            this._url = url;
            if (!context) {
                if (!AudioContext) {
                    throw "Sorry, your browser doesn't support WebAudio";
                }
                this.context = new AudioContext();
            } else {
                this.context = context;
            }
            writeline("LogSendAudio: url=[" + url + "], contentType=[audio/basic]");
        }
        WebAudioSource.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        WebAudioSource.prototype.addEventListener = function (type, listener, useCapture) {
        };
        WebAudioSource.prototype.dispatchEvent = function (evt) {
            return false;
        };
        WebAudioSource.prototype.connect = function (destination, output, input) {
            var _this = this;
            var request;
            this._destination = destination;
            if (null != this._aBuffer && this._aBuffer.byteLength > 0) {
                this.bufferReceived();
                return;
            }
            writeline("connect " + this);
            request = new XMLHttpRequest();
            request.open('GET', this._url, true);
            request.responseType = 'arraybuffer';
            request.onload = function () {
                if (request.readyState == 4 && request.status !== 200) {
                    _this.handleEnd(request.status);
                } else {
                    _this._aBuffer = request.response;
                    _this.bufferReceived();
                }
            };
            request.send();
        };
        WebAudioSource.prototype.disconnect = function () {
            var dest = this._destination;
            if (null == dest) {
                return;
            }
            if (this._bufferSource) {
                this._bufferSource.disconnect();
                this._bufferSource.stop();
            }
            this._started = false;
            dest.disconnect();
            this._destination = null;
            Platform.getCU().done(function (cu) {
                cu.disconnect();
            });
            if (this.onended) {
                this.onended();
            }
            dispatchAudioStop();
        };
        WebAudioSource.prototype.start = function (when, offset, duration) {
        };
        WebAudioSource.prototype.stop = function (when) {
        };
        WebAudioSource.prototype.setBuffer = function (buffer) {
            this._aBuffer = buffer;
            this.bufferReceived();
        };
        WebAudioSource.prototype.bufferReceived = function () {
            var _this = this;
            var bufferSource;
            bufferSource = this.context.createBufferSource();
            decodeAudioData(this.context, this._aBuffer, function (buffer) {
                _this._bufferSource = bufferSource;
                _this._bufferSource.buffer = buffer;
                _this.onBufferLoaded();
            }, function () {
                writeline("error decoding WebAudio");
                _this.handleEnd();
            });
        };
        WebAudioSource.prototype.handleEnd = function (err) {
            writeline("Source ended: err='" + err + "' " + this);
            this.disconnect();
        };
        WebAudioSource.prototype.onBufferLoaded = function () {
            var _this = this;
            this._bufferSource.onended = function () {
                _this.handleEnd();
            };
            this._bufferSource.connect(this._destination);
            this._bufferSource.start(0);
            this._started = true;
        };
        WebAudioSource.prototype.toString = function () {
            return this._url;
        };
        return WebAudioSource;
    })();
    Bing.WebAudioSource = WebAudioSource;    
    var ArrayBufferSource = (function () {
        function ArrayBufferSource(context) {
            this.numberOfInputs = 1;
            this.numberOfOutputs = 1;
            this.channelCount = 1;
            if (!context) {
                if (!AudioContext) {
                    throw "Sorry, your browser doesn't support WebAudio";
                }
                this.context = new AudioContext();
            } else {
                this.context = context;
            }
        }
        ArrayBufferSource.prototype.removeEventListener = function (type, listener, useCapture) {
        };
        ArrayBufferSource.prototype.addEventListener = function (type, listener, useCapture) {
        };
        ArrayBufferSource.prototype.dispatchEvent = function (evt) {
            return false;
        };
        ArrayBufferSource.prototype.connect = function (destination, output, input) {
            this._destination = destination;
            if (null != this._aBuffer && this._aBuffer.byteLength > 0) {
                this.bufferReceived();
                return;
            }
            writeline("connect " + this);
        };
        ArrayBufferSource.prototype.disconnect = function () {
            var dest = this._destination;
            if (null == dest) {
                return;
            }
            if (this._bufferSource) {
                this._bufferSource.disconnect();
                this._bufferSource.stop();
            }
            this._started = false;
            dest.disconnect();
            this._destination = null;
            Platform.getCU().done(function (cu) {
                cu.disconnect();
            });
            if (this.onended) {
                this.onended();
            }
            dispatchAudioStop();
        };
        ArrayBufferSource.prototype.start = function (when, offset, duration) {
        };
        ArrayBufferSource.prototype.stop = function (when) {
        };
        ArrayBufferSource.prototype.setBuffer = function (buffer) {
            this._aBuffer = buffer;
        };
        ArrayBufferSource.prototype.bufferReceived = function () {
            var _this = this;
            var bufferSource;
            bufferSource = this.context.createBufferSource();
            decodeAudioData(this.context, this._aBuffer, function (buffer) {
                _this._bufferSource = bufferSource;
                _this._bufferSource.buffer = buffer;
                _this.onBufferLoaded();
            }, function () {
                writeline("error decoding WebAudio");
                _this.handleEnd();
            });
        };
        ArrayBufferSource.prototype.handleEnd = function (err) {
            writeline("Source ended: err='" + err + "' " + this);
            this.disconnect();
        };
        ArrayBufferSource.prototype.onBufferLoaded = function () {
            var _this = this;
            this._bufferSource.connect(this._destination);
            this._bufferSource.start(0);
            this._started = true;
            this._bufferSource.onended = function () {
                _this.handleEnd();
            };
        };
        return ArrayBufferSource;
    })();
    Bing.ArrayBufferSource = ArrayBufferSource;    
    function SpeechMain() {
        var mediaNav = navigator;
        var acWindow = window;
        mediaNav.getUserMedia = mediaNav.getUserMedia || mediaNav.mozGetUserMedia || mediaNav.webkitGetUserMedia || mediaNav.msGetUserMedia || CreateActiveXObject("Bing.GetUserMedia");
        acWindow.AudioContext = acWindow.AudioContext || acWindow.webkitAudioContext || CreateActiveXObject("Bing.AudioContext");
        Bing._window.SpeechSynthesisUtterance = Bing._window.SpeechSynthesisUtterance || msSpeechSynthesisUtterance;
        Bing._window.SpeechRecognition = Bing.Speech;
        Bing._window.msSpeechSynthesis = new Bing.Synthesis();
        Bing._window.speechSynthesis = Bing._window.msSpeechSynthesis;
        Bing.initialize();
    }
    SpeechMain();
})(Bing || (Bing = {}));

var SpeechService = (function ($) {
    _public = {};
    let dataClient;
    let authToken;
    let subscriptionKey, recognitionMode, language;

    _public.init = function () {
        subscriptionKey = "b0923e2d4f5f4a76a2905a4f74822ec4";
        recognitionMode = Microsoft.CognitiveServices.SpeechRecognition.SpeechRecognitionMode.shortPhrase;
        language = "en-us";

        client = Microsoft.CognitiveServices.SpeechRecognition.SpeechRecognitionServiceFactory.createMicrophoneClient(
            recognitionMode,
            language,
            subscriptionKey);
    }

    _public.startListening = function (onSuccess, onError) {
        if (!onSuccess || !onError) { return; }

        client.startMicAndRecognition();
        client.onFinalResponseReceived = onSuccess;
        client.onError = onError;
    }

    _public.stopListening = function () {
        client.endMicAndRecognition();
    }

    return _public;
})(jQuery);

$(function () {
    SpeechService.init();
});
; var BingSearchService = (function ($) {
    _public = {};

    var bingSearchKey, bingEndpoint;

    _public.search = function(query, onSuccess, onError) {
        let searchQuery = bingEndPoint;
        searchQuery += "?q=" + query;
        searchQuery += "&safeSearch=Strict";
        searchQuery += "&count=1";

        return $.ajax({
            url: searchQuery,
            beforeSend: addBingSearchHeaders,
            type: "GET",
            success: onSuccess,
            error: onError
        });
    }

    function addBingSearchHeaders(xhrObj) {
        xhrObj.setRequestHeader("Content-Type", "multipart/form-data");
        xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", bingSearchKey);
    }

    _public.init = function () {
        bingSearchKey = "700422f67c0c4b81b8f90e438d29b5a2";
        bingEndPoint = "https://api.cognitive.microsoft.com/bing/v5.0/images/search";
    }

    return _public;
})(jQuery);

$(function () {
    BingSearchService.init();
});
;
var TireService = (function($) {
    _public = {};

    var optionEndPoint;
    var TOKEN_KEY = 'tires-token';

    function addAuthorizationHeader(xhrObj) {
        xhrObj.setRequestHeader('Authorization', 'Bearer ' + getToken());
    }

    _public.getOptions = function(year, make, model, onSuccess, onError) {
        let optionQuery = optionEndPoint;
        optionQuery += "?carManufacturer=" + make;
        optionQuery += "&carYear=" + year;
        optionQuery += "&carModel=" + model;

        return $.ajax({
            url: optionQuery,
            type: "GET",
            beforeSend: addAuthorizationHeader,
            success: onSuccess,
            fail: onError
        });
    }

    _public.getTires = function(width, aspectRatio, rimDiameter, onSuccess, onError) {
        let params = new TireSearchParams(width, aspectRatio, rimDiameter);
        let url = encodeURI("https://ta3.tiresanytime.com/api/TireSearchingApi/SearchTires?searchParams=" + JSON.stringify(params));

        return $.ajax({
            url: url,
            type: "GET",
            beforeSend: addAuthorizationHeader,
            success: onSuccess,
            error: onError
        })
    }

    _public.login = function() {
        let authParams = 'grant_type=password&dealerId=ADF3ABD9-0C2C-4176-A117-03EAEDDA4AB0';
        let tokenUrl = 'https://ta3.tiresanytime.com/Token';
        // Response format: {"access_token":"-TpRRNeBLeTMY-BGANtR51uXqCAh_3MrnV1GPRbj8kZOCq2Nt4Py8-pVXnk-RmrUFx2qLlP57u7Ypg-jXaLKp0zm5tfZ3CDY7Qd22Rx2ieogkWYyZdiDdLcBu2FUZ5-EVbPeaa7oSaVqlsiR-1fHbLKGS05Zvw3WT1V7d9cN_IajjuXxauGpz9UAVsqYX_vkxNMtPnbc0U6wuNRNGqcnS8haBxoJhFeozQpT8gq7svWw1znbIo4sdxhgtRsZEN41","token_type":"bearer","expires_in":1209599}

        return $.ajax({
            url: tokenUrl,
            type: 'POST',
            data: authParams,
            success: saveToken,
            onError: onFailedLogin
        });
    }

    function onFailedLogin() {
        console.error('Failed to login to Tires Anytime API');
    }

    function saveToken(data) {
        if (data && data.access_token) {
            localStorage.setItem(TOKEN_KEY, data.access_token);
        }
    }

    function getToken() {
        return localStorage.getItem(TOKEN_KEY);
    }

    _public.init = function() {
        optionEndPoint = "https://ta3.tiresanytime.com/api/TireSearchingApi/GetOptions";
        this.login(); // always login when page is loaded, so a refresh will fix any auth errors without needing to write retry logic in PoC
    }

    return _public;
})(jQuery);

$(function() {
    TireService.init();
});
; var State = (function ($) {
    var _public = {};

    _public.Year;
    _public.Make;
    _public.Model;
    _public.Option;

    let lastSpeechEvent;

    let States = {
        LISTENING_FOR_VEHICLE: "listeningForVehicle",
        SEARCHING_FOR_VEHICLE: "searchingForVehicle",
        VEHICLE_FOUND: "vehicleFound",
        LISTENING_FOR_OPTION: "listeningForOption",
        SEARCHING_FOR_TIRES: "searchingForTires",
        ERROR_LISTENING_FOR_VEHICLE: "errorListeningForVehicle",
        GOING_BACK: "goingBack",
        INITIAL_STATE: "initState"
    }
    let _state = null;
    

    function onTiresAnytimeCommandReceived(event, token, transcript) {
        lastSpeechEvent = Date.now();
        //VehiclePage.startListeningForVehicle();
        Visualizer.changeColor("red");
        $(document).trigger('listening-for-vehicle');
    }

    function onChromeAudioOnResult(event, transcript, confidence) {
        switch (_state) {
            case States.LISTENING_FOR_VEHICLE:
            case States.ERROR_LISTENING_FOR_VEHICLE:
                VehiclePage.getVehicleFromText(transcript);
                break;
            case States.LISTENING_FOR_OPTION:
                OptionsPage.getOptionFromText(transcript);
                break;
            default:
                break;
        }
    }

    const VEHICLE_MIC_LISTENING = "Listening for vehicle...";
    function onListeningForVehicle() {
        _state = States.LISTENING_FOR_VEHICLE;
        Status.status(VEHICLE_MIC_LISTENING);
        Visualizer.changeColor("red");
    }

    function onErrorListeningForVehicle() {
        _state = States.ERROR_LISTENING_FOR_VEHICLE;
    }

    const VEHICLE_SEARCHING = "Searching for {0}...";
    function onSearchingForVehicle(event, year, make, model) {
        _state = States.SEARCHING_FOR_VEHICLE;
        let msg = VEHICLE_SEARCHING.replace("{0}", year + " " + make + " " + model);
        Status.status(msg);
    }

    function onVehicleFound(e, year, make, model) {
        _state = States.VEHICLE_FOUND;

        _public.Year = year;
        _public.Make = make;
        _public.Model = model;
        _public.Option = undefined; // clear any saved option as we're searching for a new vehicle

        let route = '#options';
        route += '/' + year;
        route += '/' + make;
        route += '/' + model;

        parent.history.pushState(null, null, window.localStorage.href);
        window.location.hash = route;
    }

    function onOptionFound(e, option) {
        if (!option || !option.SecWidth || !option.AspRatio || !option.Rim) {
            Status.errors(["Could not determine width, aspect ratio, and rim diameter from option " + option.AutoOption]);
            return;
        }
        _public.Option = option;

        let
            width = option.SecWidth,
            aspRatio = option.AspRatio,
            rimDiameter = option.Rim;

        let route = '#tires';
        route += '/' + width;
        route += '/' + aspRatio;
        route += '/' + rimDiameter;

        parent.history.pushState(null, null, window.location.href);
        window.location.hash = route;
    }

    const OPTION_MIC_LISTENING = "Listening for option...";
    const OPTION_SEARCHING = "Searching for tires...";
    function onListeningForOption() {
        _state = States.LISTENING_FOR_OPTION;
        Status.status(OPTION_MIC_LISTENING);
        timeOfListeningForOption = Date.now();
        Visualizer.changeColor("red");
    }
    function onSearchingForTires() {
        let msg = OPTION_SEARCHING;
        if (_public.Option && _public.Option.AutoOption && _public.Option.TireSizeField) {
            let text = _public.Option.AutoOption + " (" + _public.Option.TireSizeField + ")";
            msg = "Searching for " + text;
        }
        Status.status(msg);
    }

    var previousPage;
    function onPageRenderBegin(e, prevPage) {
        if (prevPage && prevPage.length > 0) {
            previousPage = prevPage;
        }
    }

    function onPageRenderComplete(e, renderedPage) {
        if (previousPage) {
            previousPage.addClass('leaving-page').removeClass('active-page');
            renderedPage.addClass('active-page');
            setTimeout(function () {
                previousPage.removeClass('leaving-page');
            }, 1500);
        } else {
            renderedPage.addClass('active-page');
            previousPage = renderedPage;
        }

        if (renderedPage && renderedPage[0] && renderedPage[0].id)
        switch (renderedPage[0].id) {
            case "options-page":
                $(document).trigger('listening-for-option')
                break;
            default:
                break;
        }
    }

    function onBackCommandReceived(event, token, transcript) {
        _state = States.GOING_BACK;
        parent.history.back();
        return false;
    }

    function onResetCommandReceived(event, token, transcript) {
        _state = States.INITIAL_STATE;
        if (window.location.hash == "") {
            window.location.reload(true);
        } else {
            window.location.hash = "";
        }
    }

    function onHelpCommandReceived(event) {
        $.colorbox({
            width: '80%',
            height: '80%',
            inline: true,
            href: '#help',
            closeButton: false,
            returnFocus: false
        });
    }

    function onCloseCommandReceived(event) {
        $.colorbox.close();
    }

    _public.init = function () {
        $(document).on('tires-anytime-command-received', onTiresAnytimeCommandReceived);

        $(document).on('chrome-audio-onresult', onChromeAudioOnResult);

        $(document).on('listening-for-vehicle', onListeningForVehicle);
        $(document).on('error-listening-for-vehicle', onErrorListeningForVehicle);
        $(document).on('searching-for-vehicle', onSearchingForVehicle);
        $(document).on('vehicle-found', onVehicleFound);

        $(document).on('listening-for-option', onListeningForOption);
        $(document).on('option-found', onOptionFound);
        $(document).on('searching-for-tires', onSearchingForTires);

        $(document).on('page-render-begin', onPageRenderBegin);
        $(document).on('page-render-complete', onPageRenderComplete);

        $(document).on('back-command-received', onBackCommandReceived);
        $(document).on('reset-command-received', onResetCommandReceived);

        $(document).on('help-command-received', onHelpCommandReceived);
        $(document).on('close-command-received', onCloseCommandReceived);

        $(document).on('keypress', (event) => {
            if (event.which === 63) {
                $(document).trigger('help-command-received');
            }
        });
    }

    return _public;
})(jQuery);

$(function () {
    State.init();
});
function TireSearchParams(width, aspRatio, rim) {
    this.DealerGroupId = "ADF3ABD9-0C2C-4176-A117-03EAEDDA4AB0";
    this.DealerId = 2674;
    if (width) {
        this.Width = width.toString();
    } else {
        this.Width = "235";
    }

    if (aspRatio) {
        this.AspRatio = aspRatio.toString();
    } else {
        this.AspRatio = "75";
    }

    if (rim) {
        this.Rim = rim.toString();
    } else {
        this.Rim = "16";
    }

    this.LoadRating = "";
    this.SpeedRation = "";
    this.Width2 = null;
    this.AspRation2 = null;
    this.Rim2 = null;
    this.LoadRating2 = "";
    this.SpeedRating2 = ""
}
var ManufacturerData = (function($) {
    var manufacturerData =
        [
      {
          "token": "acura",
          "value": "Acura"
      },
      {
          "token": "alfa romeo",
          "value": "Alfa Romeo"
      },
      {
          "token": "am general",
          "value": "AM General"
      },
      {
          "token": "amg",
          "value": "AM General"
      },
      {
          "token": "american motors",
          "value": "American Motors"
      },
      {
          "token": "aston martin",
          "value": "Aston Martin"
      },
      {
          "token": "audi",
          "value": "Audi"
      },
      {
          "token": "avanti",
          "value": "Avanti"
      },
      {
          "token": "bentley",
          "value": "Bentley"
      },
      {
          "token": "bertone",
          "value": "Bertone"
      },
      {
          "token": "bmw",
          "value": "BMW"
      },
      {
          "token": "bugatti",
          "value": "Bugatti"
      },
      {
          "token": "buick",
          "value": "Buick"
      },
      {
          "token": "cadillac",
          "value": "Cadillac"
      },
      {
          "token": "checker",
          "value": "Checker"
      },
      {
          "token": "chevy",
          "value": "Chevrolet"
      },
      {
          "token": "chevrolet",
          "value": "Chevrolet"
      },
      {
          "token": "chrysler",
          "value": "Chrysler"
      },
      {
          "token": "coda",
          "value": "Coda"
      },
      {
          "token": "daewoo",
          "value": "Daewoo"
      },
      {
          "token": "daihatsu",
          "value": "Daihatsu"
      },
      {
          "token": "delorean",
          "value": "DeLorean"
      },
      {
          "token": "dodge",
          "value": "Dodge"
      },
      {
          "token": "eagle",
          "value": "Eagle"
      },
      {
          "token": "ferrari",
          "value": "Ferrari"
      },
      {
          "token": "fiat",
          "value": "Fiat"
      },
      {
          "token": "fisker",
          "value": "Fisker"
      },
      {
          "token": "ford",
          "value": "Ford"
      },
      {
          "token": "freightliner",
          "value": "Freightliner"
      },
      {
          "token": "geo",
          "value": "Geo"
      },
      {
          "token": "gmc",
          "value": "GMC"
      },
      {
          "token": "honda",
          "value": "Honda"
      },
      {
          "token": "hummer",
          "value": "Hummer"
      },
      {
          "token": "hyundai",
          "value": "Hyundai"
      },
      {
          "token": "infiniti",
          "value": "Infiniti"
      },
      {
          "token": "international",
          "value": "International"
      },
      {
          "token": "isuzu",
          "value": "Isuzu"
      },
      {
          "token": "jaguar",
          "value": "Jaguar"
      },
      {
          "token": "jeep",
          "value": "Jeep"
      },
      {
          "token": "kia",
          "value": "Kia"
      },
      {
          "token": "lamborghini",
          "value": "Lamborghini"
      },
      {
          "token": "lancia",
          "value": "Lancia"
      },
      {
          "token": "land rover",
          "value": "Land Rover"
      },
      {
          "token": "lexus",
          "value": "Lexus"
      },
      {
          "token": "lincoln",
          "value": "Lincoln"
      },
      {
          "token": "lotus",
          "value": "Lotus"
      },
      {
          "token": "maserati",
          "value": "Maserati"
      },
      {
          "token": "maybach",
          "value": "Maybach"
      },
      {
          "token": "mazda",
          "value": "Mazda"
      },
      {
          "token": "mclaren",
          "value": "McLaren"
      },
      {
          "token": "mercedes-benz",
          "value": "Mercedes-Benz"
      },
      {
          "token": "mercury",
          "value": "Mercury"
      },
      {
          "token": "merkur",
          "value": "Merkur"
      },
      {
          "token": "mg",
          "value": "MG"
      },
      {
          "token": "mini",
          "value": "Mini"
      },
      {
          "token": "mitsubishi",
          "value": "Mitsubishi"
      },
      {
          "token": "mobility ventures",
          "value": "Mobility Ventures"
      },
      {
          "token": "nissan",
          "value": "Nissan"
      },
      {
          "token": "oldsmobile",
          "value": "Oldsmobile"
      },
      {
          "token": "panoz",
          "value": "Panoz"
      },
      {
          "token": "peugeot",
          "value": "Peugeot"
      },
      {
          "token": "pininfarina",
          "value": "Pininfarina"
      },
      {
          "token": "plymouth",
          "value": "Plymouth"
      },
      {
          "token": "pontiac",
          "value": "Pontiac"
      },
      {
          "token": "porsche",
          "value": "Porsche"
      },
      {
          "token": "ram",
          "value": "RAM"
      },
      {
          "token": "renault",
          "value": "Renault"
      },
      {
          "token": "rolls-royce",
          "value": "Rolls-Royce"
      },
      {
          "token": "saab",
          "value": "Saab"
      },
      {
          "token": "saleen",
          "value": "Saleen"
      },
      {
          "token": "saturn",
          "value": "Saturn"
      },
      {
          "token": "scion",
          "value": "Scion"
      },
      {
          "token": "smart",
          "value": "Smart"
      },
      {
          "token": "srt",
          "value": "SRT"
      },
      {
          "token": "sterling",
          "value": "Sterling"
      },
      {
          "token": "subaru",
          "value": "Subaru"
      },
      {
          "token": "suzuki",
          "value": "Suzuki"
      },
      {
          "token": "tesla",
          "value": "Tesla"
      },
      {
          "token": "toyota",
          "value": "Toyota"
      },
      {
          "token": "triumph",
          "value": "Triumph"
      },
      {
          "token": "vw",
          "value": "Volkswagen"
      },
      {
          "token": "volkswagen",
          "value": "Volkswagen"
      },
      {
          "token": "volvo",
          "value": "Volvo"
      },
      {
          "token": "vpg",
          "value": "VPG"
      },
      {
          "token": "yugo",
          "value": "Yugo"
      }
        ];

    return manufacturerData;
})(jQuery);
var NumbersToText = (function ($) {
    var numbersToText =
        [
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen",
            "twenty",
            "twenty-one",
            "twenty-two",
            "twenty-three",
            "twenty-four",
            "twenty-five",
            "twenty-six",
            "twenty-seven",
            "twenty-eight",
            "twenty-nine",
            "thirty",
            "thirty-one",
            "thirty-two",
            "thirty-three",
            "thirty-four",
            "thirty-five",
            "thirty-six",
            "thirty-seven",
            "thirty-eight",
            "thirty-nine",
            "forty",
            "forty-one",
            "forty-two",
            "forty-three",
            "forty-four",
            "forty-five",
            "forty-six",
            "forty-seven",
            "forty-eight",
            "forty-nine",
            "fifty"
        ];

    return numbersToText;
})(jQuery);
var LocalSearch = (function($) {
    let _public = {};

    var manufacturers = {};
    var years = {};
    var manufacturerData = ManufacturerData;
    var numbersToText = NumbersToText;
    var optionsIndex = {};

    function buildSearchIndex() {
        var lowBoundYear = 1920;
        var nextYear = (new Date()).getFullYear() + 1;
        for (let i = lowBoundYear; i <= nextYear; i++) {
            let yearStr = i.toString();
            let twoDigitYearStr = yearStr.substr(2, 2);
            years[yearStr] = yearStr;
            years[twoDigitYearStr] = yearStr;
        }

        for (let i = 0; i < manufacturerData.length; i++) {
            let manufacturer = manufacturerData[i];
            manufacturers[manufacturer.token] = manufacturer.value;
        }
    }

    /**
     * Build a search index from the given options data
     */
    _public.setOptions = function(data) {
        optionsIndex = {};
        $.each(data, function (i, option) {
            let optionNumber = i + 1;
            optionsIndex[optionNumber] = option;
            optionsIndex["option " + optionNumber] = option;
            optionsIndex["number " + optionNumber] = option;
            optionsIndex["option number " + optionNumber] = option;
            let textNumber = numbersToText[optionNumber];
            optionsIndex[textNumber] = option;
            optionsIndex["option " + textNumber] = option;
            optionsIndex["number " + textNumber] = option;
            optionsIndex["option number " + textNumber] = option;
            optionsIndex[option.AutoOption.toLowerCase()] = option;
            
            // Oddballs
            if (optionNumber === 4) {
                optionsIndex["for"] = option;
                optionsIndex["option for"] = option;
                optionsIndex["option number for"] = option;
            }
            if (optionNumber === 3) {
                optionsIndex["free"] = option;
                optionsIndex["option free"] = option;
                optionsIndex["option number free"] = option;
            }
        });
    }

    /**
     * Get the full option object from the search index based on the given token
     */
    _public.findOption = function(token) {
        return optionsIndex[token.toLowerCase()];
    }

    /** 
     * Get a year from the search index based on the given token
     */
    _public.findYear = function(token) {
        return years[token.toLowerCase()];
    }

    /** 
     * Get a Make from the search index based on the given token
     */
    _public.findMake = function(token) {
        return manufacturers[token.toLowerCase()];
    }

    _public.init = function () {
        buildSearchIndex();
    }

    return _public;
})(jQuery);

$(function() {
    LocalSearch.init();
});
;var Status = (function ($) {
    var _public = {};
    var $root, statusUI, errorUI;

    _public.errors = function(errors) {
        if (!errors || !errors.length || errors.length <= 0) { return; }

        let errorsHtml = "";
        $.each(errors, function (i, error) {
            errorsHtml += error + "</br>\n";
        });

        statusUI.hide();
        errorUI.html(errorsHtml).show();
    }

    _public.status = function (message) {
        errorUI.hide();
        statusUI.text(message).show();
    }

    _public.clear = function () {
        statusUI.hide();
        errorUI.hide();
    }

    _public.init = function () {
        $root = $('#status');
        statusUI = $root.find('#notification');
        errorUI = $root.find('#errors');
        _public.clear();
    }

    return _public;
})(jQuery);

$(function () {
    Status.init();
})
;var VehiclePage = (function ($) {
    var _public = {};
    var $root;
    var vehicle;

    _public.render = function () {
        $(document).trigger('page-render-begin', [$('.active-page')]);
        Status.clear();
        $(document).trigger('page-render-complete', [$root]);
    }

    function parseText(text) {
        let errors = [];
        let tokens = text.split(" ");

        var vehicle = {};
        if (tokens.length <= 0) {
            console.log("No results found");
            return;
        }
        vehicle.year = LocalSearch.findYear(tokens[0]);

        let modelIndex = 2;
        if (tokens.length > 1) {
            vehicle.make = LocalSearch.findMake(tokens[1]);
            if (!vehicle.make && tokens.length > 2) {
                // combine second and third tokens; maybe a two part name for make
                vehicle.make = LocalSearch.findMake(tokens[1] + " " + tokens[2]);
                modelIndex++;
            }
        }

        if (tokens.length > modelIndex) {
            let firstModelToken = tokens[modelIndex];
            let modelStartIndex = text.lastIndexOf(firstModelToken);
            vehicle.model = text.substr(modelStartIndex);
            vehicle.model = vehicle.model.replace(" ", "").replace(".", "").replace("?", "").replace(",", "");
        }

        if (vehicle.year) {
            console.log("Year: " + vehicle.year);
        } else {
            console.log("Could not determine year");
        }

        if (vehicle.make) {
            console.log("Make: " + vehicle.make);
        } else {
            console.log("Could not determine make");
        }

        if (vehicle.model) {
            console.log("Model: " + vehicle.model);
        } else {
            console.log("Could not determine model");
        }

        if (!vehicle || !vehicle.year || !vehicle.make || !vehicle.model) {
            errors.push("We heard: " + text + ". Please try again.");
            Status.errors(errors);
        }

        return vehicle;
    }

    _public.getVehicleFromText = function (text) {
        var vehicle = parseText(text);
        if (vehicle && vehicle.year && vehicle.make && vehicle.model) {
            $(document).trigger('vehicle-found', [vehicle.year, vehicle.make, vehicle.model]);
        } else {
            Status.errors(errors);
            $(document).trigger('error-listening-for-vehicle');
        }
    }

    _public.init = function () {
        $root = $('#vehicle-page');
    }
    return _public;
})(jQuery);

$(function () {
    VehiclePage.init();
});
;var OptionsPage = (function ($) {
    var _public = {};

    var $root, $options, $thumbnail, $carTitle;
    var foundOptions = false; // It's an AJAX success to find no options, so manually track it
    _public.render = function (year, make, model) {
        $(document).trigger('page-render-begin', [$('.active-page')]);
        // If any option is saved we're not searching for a new vehicle's options
        // and thus don't need to update the state of this page at all.
        if (!State.Option) {
            clearOptionsPageUI();
            $(document).trigger('searching-for-vehicle', [year, make, model]);

            let carDesc = year + " " + make + " " + model;
            $carTitle.text(carDesc);

            $.when( 
                BingSearchService.search(carDesc, onImageSearchSuccess, onImageSearchError),
                TireService.getOptions(year, make, model, onOptionsSuccess, onOptionsError)
            ).done(function(imageSearch, optionSearch) {
                if (foundOptions) {
                    pageRenderComplete();
                }
            });
        } else {
            pageRenderComplete();
        }
    }

    function pageRenderComplete() {
        Status.clear();
        $(document).trigger('page-render-complete', [$root]);
    }

    function onImageSearchSuccess(data) {
        console.log(data);
        if (data && data.value && data.value.length > 0 && data.value[0].thumbnailUrl) {
            let url = data.value[0].thumbnailUrl;
            $thumbnail.attr('src', url).show();
        }
    }

    function onImageSearchError() {
        Status.errors(["Error occurred searching for vehicle image"]);
    }

    function onOptionsSuccess(data) {
        if (data && data.length > 0) {
            foundOptions = true;
            $options.empty();

            $.each(data, function (i, data) {
                let text = data.AutoOption + " (" + data.TireSizeField + ")";
                $options.append($('<li>', {
                    id: data.AutoOption,
                    text: text
                }));
            });
            console.log(data);
            LocalSearch.setOptions(data);
        } else {
            onOptionsError();
        }
    }

    function onOptionsError() {
        foundOptions = false;
        Status.errors(["No options found for this vehicle. Say \"Reset\" to try again."]);
    }

    function parseText(text) {
        // This was because of weird characters Azure would return, no harm in leaving it in though.
        text = text.replace("?", "").replace(".", "").replace(",", "");

        let option = LocalSearch.findOption(text);
        if (!option) {
            Status.errors(["Could not find option - We heard: " + text + ". Please try again."]);
            return;
        }
        return option;
    }
    _public.getOptionFromText = function (text) {
        var option = parseText(text);
        if (option) {
            $(document).trigger('option-found', [option])
        }
    }

    function clearOptionsPageUI() {
        $options.empty();
        $thumbnail.attr('src', '').hide();
    }

    _public.init = function () {
        $root = $('#options-page');
        $options = $root.find('#options');
        $thumbnail = $root.find('#car-thumbnail');
        $carTitle = $root.find('#car');
    }

    return _public;
})(jQuery);

$(function () {
    OptionsPage.init();
})
;var TiresPage = (function ($) {
    var _public = {};
    var $root, $tires;
    var foundTires = false; // No results is still a successful AJAX call, so manually track results

    _public.render = function (width, aspectRatio, rimDiameter) {
        $(document).trigger('page-render-begin', [$('.active-page')]);
        clearTiresUI();
        $(document).trigger('searching-for-tires');

        $.when(
            TireService.getTires(width, aspectRatio, rimDiameter, onTiresSuccess, onTiresError)
        ).done(function () {
            if (foundTires) {
                $(document).trigger('page-render-complete', [$root]);
            }
        });
    }

    function clearTiresUI() {
        $tires.empty();
    }

    function setTireResults(results) {
        let best, better, good;
        if (results.length > 0) {
            best = results[0];
        }
        if (results.length > 1) {
            better = results[1];
        }
        if (results.length > 2) {
            good = results[2];
        }

        clearTiresUI();
        populateTireResultTemplate('Best', best);
        populateTireResultTemplate('Better', better);
        populateTireResultTemplate('Good', good);
    }

    function populateTireResultTemplate(tireClass, data) {
        if (!data) { return; }
        if (!tireClass) { return; }

        let tiresContainer = document.getElementById('tires');
        let viewModel = {};
        viewModel.tireClass = tireClass;
        viewModel.logoImagePath = data.BrandImageUrl || "";
        viewModel.tireImagePath = data.ImageUrl || "";
        viewModel.productName = data.ProductName;
        viewModel.tireDescription = data.TireDescription;
        viewModel.tireSize = data.TireSizeField || data.DisplayTireSize || data.TireSize;

        var ctx = new Stamp.Context();
        var expanded = Stamp.expand(ctx.import('tire'), viewModel);
        Stamp.appendChildren(tiresContainer, expanded);
    }

    function onTiresSuccess(data) {
        console.log(data);
        foundTires = false
        if (!data || !data.AdditionalTires || !data.AdditionalTires.TireResult) {
            Status.errors(["Cannot interpret tire results"]);
        } else if (data.AdditionalTires.TireResult.length <= 0) {
            let name = State.Option.AutoOption || "";
            let size = State.Option.TireSizeField || "";
            Status.errors(["No tires found for option " + name + " " + size]);
        } else {
            foundTires = true;
            setTireResults(data.AdditionalTires.TireResult);
            Status.clear();
        }
    }

    function onTiresError(request, status, error) {
        foundTires = false;
        Status.errors(["Error finding tires: ", error]);
    }

    _public.init = function () {
        $root = $('#tires-page');
        $tires = $root.find('#tires');
    }

    return _public;
})(jQuery);

$(function () {
    TiresPage.init();
});
;var Router = (function ($) {
    var _public = {};

    let routes = {
        "#vehicle": function(params) {
            VehiclePage.render();
        },
        "#options": function (params) {
            let year, make, model;
            [year, make, model] = params.split('/');
            OptionsPage.render(year, make, model);
        },
        "#tires": function (params) {
            let width, aspRatio, rimDiameter;
            [width, aspRatio, rimDiameter] = params.split('/');
            TiresPage.render(width, aspRatio, rimDiameter);
        },
        "default" : VehiclePage.render
    };

    _public.navigateTo = function(route) {
        let defaultPageFunc = routes["default"];
        if (route && route.trim().length > 0) {
            let routeToken = route.split('/')[0];
            let params = route.split(routeToken)[1];
            if (params && params.length > 2) {
                params = params.substr(params.indexOf('/') + 1);
                if (params.lastIndexOf('/') == params.length - 1) {
                    params = params.substr(0, params.length - 1);
                }
            }
            let pageFunc = routes[routeToken];
            if (pageFunc) {
                pageFunc(params);
            }
        } else {
            defaultPageFunc();
        }
    }
    
    _public.init = function () {
        $(window).on('hashchange', function () {
            // On every hash change the render function is called with the new hash.
            // This is how the navigation of our app happens.
            _public.navigateTo(decodeURI(window.location.hash));
        });

        $(window).trigger('hashchange');
    }
    return _public;
})(jQuery);

$(function () {
    Router.init();
});
; var Visualizer = (function ($) {

    let _public = {};
    let userMedia;

    let canvas,
        audioCtx,
        analyser,
        canvasCtx,
        drawVisual,
        gainNode;
    let source,
        _stream;

    const RED = "red",
        GRAY = "gray";

    let barColor = RED;

    function visualize() {
        WIDTH = canvas.width;
        HEIGHT = canvas.height;

        var visualSetting = "frequencybars"; //visualSelect.value;
        console.log('canvas {width: ' + WIDTH + ', height: ' + HEIGHT);
        console.log(visualSetting);

        if (visualSetting == "frequencybars") {
            analyser.fftSize = 512;
            var bufferLength = analyser.frequencyBinCount;
            console.log(bufferLength);
            var dataArray = new Uint8Array(bufferLength);

            canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

            function draw() {
                drawVisual = requestAnimationFrame(draw);

                analyser.getByteFrequencyData(dataArray);

                canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

                var barWidth = (WIDTH / bufferLength) * 2.5;
                var barHeight;
                var x = 0;

                for (var i = 0; i < bufferLength; i++) {
                    barHeight = dataArray[i] * 2;

                    if ( barColor == RED) {
                        canvasCtx.fillStyle = 'rgb(' + (barHeight + 100) + ',50,50)';
                    } else if ( barColor == GRAY ) {
                        canvasCtx.fillStyle = 'rgb(' + (barHeight + 100) + ',' + (barHeight + 100) + ',' + (barHeight + 100) + ')';
                    }
                    canvasCtx.fillRect(x, HEIGHT - barHeight / 2, barWidth, barHeight / 2);

                    x += barWidth + 1;
                }
            };

            draw();

        } else if (visualSetting == "off") {
            canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
            canvasCtx.fillStyle = "red";
            canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
        }
    }

    _public.init = function () {
        // fork getUserMedia for multiple browser versions, for those
        // that need prefixes
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        //navigator.mediaDevices.getUserMedia = (navigator.mediaDevices.getUserMedia || navigator.mediaDevices.webkitGetUserMedia || navigator.mediaDevices.mozGetUserMedia || navigator.mediaDevices.msGetUserMedia);

        // set up forked web audio context, for multiple browsers
        // window. is needed otherwise Safari explodes
        audioCtx = new (window.AudioContext || window.webkitAudioContext)();

        //set up the different audio nodes we will use for the app
        analyser = audioCtx.createAnalyser();
        analyser.minDecibels = -90;
        analyser.maxDecibels = -10;
        analyser.smoothingTimeConstant = 0.85;
        gainNode = audioCtx.createGain();

        // set up canvas context for visualizer
        canvas = document.querySelector('.visualizer');
        canvasCtx = canvas.getContext("2d");
        drawVisual;

        var intendedWidth = $('.container').width();
        canvas.setAttribute('width', intendedWidth);

        _public.start();
    }

    
    _public.changeColor = function(newColor) {
        if (newColor == RED || newColor == GRAY) {
            barColor = newColor;
        }
    }

    _public.start = function () {
        //main block for doing the audio recording
        if (navigator.getUserMedia) {
            console.log('getUserMedia supported.');
            navigator.getUserMedia(
               // constraints - only audio needed for this app
               {
                   audio: true
               },

               // Success callback
               function (stream) {
                   _stream = stream;
                   source = audioCtx.createMediaStreamSource(stream);
                   source.connect(analyser);
                   analyser.connect(gainNode);
                   gainNode.gain.value = 0;
                   gainNode.connect(audioCtx.destination);

                   visualize();
               },

               // Error callback
               function (err) {
                   console.log('The following gUM error occured: ' + err);
               }
            );
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
    }

    _public.stop = function () {
        if (_stream && _stream.getTracks) {
            let track = _stream.getTracks()[0];  // if only one media track
            if (track) {
                track.stop();
            }
        }
    }

    return _public;

})(jQuery);

$(() => {
    Visualizer.init();
})
;var ChromeSpeech = (($) => {
    let _public = {};

    let SpeechRecognition,
        SpeechGrammarList;

    let tiresAnytimeCommandTokens = ["tires", "fires", "anytime", "time"];
    let backCommandTokens = ["back", /* Rhymes */ "mac"];
    let resetCommandTokens = ["reset", "restart", "reload", /* Swears */ "f***"];
    let helpCommandTokens = ["help", /*Rhymes*/ "health"];
    let closeCommandTokens = ["health", "close", "done", "exit", /* Rhymes */ "lowe's", "clothes", "dumb"];

    let recognition,
        confidence,
        transcript;

    _public.init = () => {
        SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
        SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;

        recognition = new SpeechRecognition();
        recognition.continuous = false;
        recognition.interimResults = false;
        
        let allCommands = tiresAnytimeCommandTokens.concat(
            backCommandTokens,
            resetCommandTokens,
            helpCommandTokens,
            closeCommandTokens);
        let grammar = '#JSGF V1.0; grammar phrase; public <phrase> = ' + allCommands.join(' | ') + ';';
        let recognitionList = new SpeechGrammarList();
        recognitionList.addFromString(grammar, 1);
        recognition.grammars = recognitionList;

        recognition.onresult = function (event) {
            console.log('chrome-speech.onresult');
            if (event.results && event.results.length > 0 && event.results[0].length > 0) {
                confidence = event.results[0][0].confidence;
                transcript = event.results[0][0].transcript;
                let tokens = transcript.split(' ');
                console.log(transcript + " (" + confidence + ")");

                let commandTriggered = false;
                for (let i = 0; i < tokens.length; i++) {
                    let token = tokens[i].toLowerCase();

                    if (helpCommandTokens.includes(token)) {
                        $(document).trigger('help-command-received');
                        return;
                    }
                    if (closeCommandTokens.includes(token)) {
                        $(document).trigger('close-command-received');
                        return;
                    }
                    if (tiresAnytimeCommandTokens.includes(token)) {
                        $(document).trigger('tires-anytime-command-received', [token, transcript]);
                        return;
                    }
                    if (backCommandTokens.includes(token)) {
                        $(document).trigger('back-command-received', [token, transcript]);
                        return;
                    }
                    if (resetCommandTokens.includes(token)) {
                        $(document).trigger('reset-command-received', [token, transcript]);
                        return;
                    }
                }

                $(document).trigger('chrome-audio-onresult', [transcript, confidence]);
            }
        }

        recognition.onend = function () {
            recognition.start(); // keep chrome listening forever
        }

        _public.start();
    }

    _public.stop = function () {
        recognition && recognition.stop();
    }

    _public.start = function () {
        recognition && recognition.start();
    }

    return _public;
})(jQuery);

$(() => {
    ChromeSpeech.init();
})